﻿namespace Xindow.Enums {

    public enum WindowState {
        Unknown = -1,
        Down = 0,
        Up = 1,
        MovingDownAuto = 2,
        MovingUpAuto = 3,
        MovingDownManual = 4,
        MovingUpManual = 5,
        PartiallyOpenAndStationary = 6,
        Cracked = 7
    }
}