﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xindow.Models;
using Xindow.Services;

namespace Xindow.ViewModels
{
    class ControlPanelViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<Window> Windows { get; set; }
        //public ObservableCollection<WindowSwitch> WindowSwitches { get; set; }
        public Command LoadWindowsCommand { get; set; }

        public ControlPanelViewModel()
        {
            Title = "Windows";

            //WindowSwitches = new ObservableCollection<WindowSwitch>();
            LoadWindowsCommand = new Command(async () => await ExecuteLoadWindowsCommand());

            //MessagingCenter.Subscribe<NewItemPage, Item>(this, "AddItem", async (obj, item) =>
            //{
            //    var newItem = item as Item;
            //    Items.Add(newItem);
            //    await DataStore.AddItemAsync(newItem);
            //});
        }

        async Task ExecuteLoadWindowsCommand()
        {
            if (IsBusy)
            {
                return;
            }

            IsBusy = true;

            try
            {
                Windows.Clear();
                var windows = await WindowDataStore.GetAllAsync(true);
                foreach (var window in windows)
                {
                    Windows.Add(window);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public IWindowDataStore<Window> WindowDataStore => DependencyService.Get<IWindowDataStore<Window>>() ?? new MockWindowDataStore();

        bool isBusy = false;
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        string title = string.Empty;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }

        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName]string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}