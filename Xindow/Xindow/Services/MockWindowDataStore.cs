﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xindow.Enums;
using Xindow.Models;

namespace Xindow.Services
{

    //TODO: Choose either Vehicle or Window for the naming of this.  It's janky as-is.  Moving on for now.
    public class MockWindowDataStore : IWindowDataStore<Window>
    {
        IList<Window> windows;

        public MockWindowDataStore()
        {
            windows = new List<Window>();
            var mockWindows = new List<Window>
            {
                new Window { Id = WindowLocation.DriverFront},
                new Window { Id = WindowLocation.PassengerFront},
                new Window { Id = WindowLocation.DriverBack},
                new Window { Id = WindowLocation.PassengerBack}
            };

            foreach (var window in mockWindows)
            {
                windows.Add(window);
            }
        }

        public async Task<bool> AddSingleAsync(Window window)
        {
            windows.Add(window);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateSingleAsync(Window window)
        {
            var oldWindow = windows.Where((Window arg) => arg.Id == window.Id).FirstOrDefault();
            windows.Remove(oldWindow);
            windows.Add(window);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteSingleAsync(WindowLocation id)
        {
            var oldWindow = windows.Where((Window arg) => arg.Id == id).FirstOrDefault();
            windows.Remove(oldWindow);

            return await Task.FromResult(true);
        }

        public async Task<Window> GetSingleAsync(WindowLocation id)
        {
            return await Task.FromResult(windows.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Window>> GetAllAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(windows);
        }
    }
}