﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xindow.Enums;

namespace Xindow.Services
{
    public interface IWindowDataStore<T>
    {
        Task<bool> AddSingleAsync(T item);
        Task<bool> UpdateSingleAsync(T item);
        Task<bool> DeleteSingleAsync(WindowLocation id);
        Task<T> GetSingleAsync(WindowLocation id);
        Task<IEnumerable<T>> GetAllAsync(bool forceRefresh = false);
    }
}
