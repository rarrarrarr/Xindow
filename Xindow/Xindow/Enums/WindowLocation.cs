﻿namespace Xindow.Enums {

    public enum WindowLocation {
        DriverFront = 0,
        PassengerFront = 1,
        DriverBack = 2,
        PassengerBack = 3
    }
}