﻿using Xindow.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Xindow.ViewModels {

    public class Vehicle : INotifyPropertyChanged {

        public Vehicle(WindowLocation location) {
            _location = location;
            AutoRollDownCommand = new Command(async () => await AutoRollDown(),
                                            () => !IsBusy);
        }

        private WindowState _state;
        private WindowLocation _location;
        private bool _isBusy = false;

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string name = "") {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public WindowState State {
            get => _state;
            set {
                _state = value;
                OnPropertyChanged();
            }
        }

        public WindowLocation Location {
            get => _location;
            set {
                _location = value;
                OnPropertyChanged();
            }
        }

        public bool IsBusy {
            get => _isBusy;
            set {
                _isBusy = value;

                OnPropertyChanged();
                AutoRollDownCommand.ChangeCanExecute();
            }
        }

        public Command AutoRollDownCommand { get; }

        public async Task AutoRollDown() {
            IsBusy = true;

            State = WindowState.MovingDownAuto;

            try {
                await Task.Delay(2000);
            } catch {
                State = WindowState.Unknown;
            }

            State = WindowState.Down;

            IsBusy = false;
        }
    }
}